package com.example.ScangunApp

import com.example.ScangunApp.DataClasses.Organization
import com.google.gson.Gson
import org.junit.Assert.assertEquals


import org.junit.Test

class JsonUnitTest {

    private val jsonString = """
        {
            "Code":"TOR",
            "Id": 101
        }
    """.trimIndent()

    @Test
    fun gsonTest(){
        val org = Gson().fromJson(jsonString, Organization::class.java)
        val expectedID = 101
        val expectedCode = "TOR"
        assertEquals("Code is " + org.Code + " but should be '"+expectedCode+"'" ,expectedCode, org.Code)
        assertEquals("Id is " + org.Id + " but should be " + expectedID, expectedID, org.Id)
    }


}