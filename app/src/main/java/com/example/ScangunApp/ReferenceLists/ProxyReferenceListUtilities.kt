package com.example.ScangunApp.ReferenceLists

import android.content.res.Resources
import java.io.BufferedReader
import java.io.InputStream


class ProxyReferenceListUtilities : IReferenceListUtilities {


    override fun GetShippingOrganizationsText(resources: Resources, baseUrl: String): String {
        val inputStream : InputStream = resources.assets.open(baseUrl)
        return inputStream.bufferedReader().use(BufferedReader::readText)
    }

    override fun GetPackagesText(resources: Resources,baseUrl: String): String {
        // Will use the other parameters later
        val inputStream : InputStream = resources.assets.open(baseUrl)
        return inputStream.bufferedReader().use(BufferedReader::readText)
    }
}