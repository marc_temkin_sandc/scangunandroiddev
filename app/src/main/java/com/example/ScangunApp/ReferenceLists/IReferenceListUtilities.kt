package com.example.ScangunApp.ReferenceLists

import android.content.res.Resources

interface IReferenceListUtilities {
    fun GetShippingOrganizationsText(resources: Resources, baseUrl:String):String
    fun GetPackagesText(resources: Resources,baseUrl:String):String
}