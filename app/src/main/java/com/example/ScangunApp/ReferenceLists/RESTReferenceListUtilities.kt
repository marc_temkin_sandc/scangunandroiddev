@file:Suppress("DEPRECATION")

package com.example.ScangunApp.ReferenceLists

import android.content.Context
import android.content.res.Resources
import android.os.Build
import androidx.annotation.RequiresApi
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.RequestFuture
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.ScangunApp.DataClasses.Organization
import com.example.ScangunApp.Singletons.CollectionsForLists
import com.google.gson.Gson
import org.json.JSONArray
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL
import java.util.concurrent.ExecutionException


private const val TAG = "OrgListFragment"

class RESTReferenceListUtilities(val context: Context) : IReferenceListUtilities {

    var strResponse= ""
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun GetShippingOrganizationsText(resources: Resources, baseUrl: String): String {
        //TODO("Not yet implemented")

        retrieveFromNetwork()
        return strResponse
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun GetPackagesText(resources: Resources,baseUrl: String): String {
      //  TODO("Not yet implemented")
        return ""
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    private fun retrieveFromNetwork()
    {

        val queue: RequestQueue = Volley.newRequestQueue(context)
        val url = "http://chi-ic01/LoadList.svc/api/organizations"

        var strResp = ""
        val stringReq = StringRequest(
            Request.Method.GET, url,
            { response ->

                strResp = response.toString()
                val jarray = JSONArray(response)
                val gson = Gson()
                val orgList = gson.fromJson(response, Array<Organization>::class.java)
            },
            { val str  =
                "That didn't work!" }
        )

        queue.add(stringReq)

        //return strResp

    }
}