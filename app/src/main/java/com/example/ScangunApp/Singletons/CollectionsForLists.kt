package com.example.ScangunApp.Singletons

import android.util.Log
import com.example.ScangunApp.DataClasses.Organization
import com.example.ScangunApp.DataClasses.ShippingPackage
import java.lang.Exception

class CollectionsForLists {




    companion object OrganizationCollection {
        var orgsList : List<Organization> = listOf()
        var pkgsList : List<ShippingPackage> = listOf()

        fun InitOrg(orgs:Array<Organization>)
        {
            orgsList = orgs.toList()
            var position = 0
            for (rec in orgsList)
                rec.position = position++
        }

        fun InitPackages(pkgs: Array<ShippingPackage>) {

            pkgsList = pkgs.toList().sortedBy { p -> p.ContainerName}

            var position = 0
            for (rec in pkgsList)
            {
                rec.position = position++
                Log.i("Excel", rec.ContainerName)
            }

        }


        fun UpdatePackages(position:Int)
        {
            if (position < 0 || position > pkgsList.size)
            {
                throw Exception("UpdatePackages: argument position $position is out-of-range")
            }

            pkgsList[position].Found = true

        }
    }
}