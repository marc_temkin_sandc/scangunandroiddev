package com.example.ScangunApp.StateMachineLogic

import com.example.ScangunApp.Fragments.FragmentWithToolbar

data class ViewMenuAction (val fragment: FragmentWithToolbar, val scanAction: ScanAction)