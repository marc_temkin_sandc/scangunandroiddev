package com.example.ScangunApp.StateMachineLogic

enum class ShippingApplicationMenu {
    None,
    Main,
    ScanBadge,
    SelectOrganization,
    ScanTripId,
    ScanPackages,
    ScanPackage,
    Back,
    ExitApp
}