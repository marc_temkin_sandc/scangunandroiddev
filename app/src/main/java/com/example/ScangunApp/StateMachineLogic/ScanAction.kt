package com.example.ScangunApp.StateMachineLogic

// Base class used for derived actions with particular states
open class ScanAction(scanOption:Boolean=false) {

    open fun Action() {}
}


class StartScan(scanOption: Boolean) : ScanAction(scanOption)
{
    override fun Action() {
        super.Action()
    }
}


class StopScan(scanOption:Boolean=false) : ScanAction(scanOption)
{
    override fun Action() {
        super.Action()
        super.Action()
    }
}
