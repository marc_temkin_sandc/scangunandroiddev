package com.example.ScangunApp.StateMachineLogic

import com.example.ScangunApp.DataClasses.AppKeyInfo
import com.example.ScangunApp.Fragments.PackagesHostFragment
import java.util.*

object StateMachine {
    fun clearScanFlags() {
        scanBadgeReceives = false
        scanTripReceives = false
        scanPackageReceives = false
    }

    // The current menu state
    var currentMenu : ShippingApplicationMenu = ShippingApplicationMenu.None

    val appKeyInfo:AppKeyInfo = AppKeyInfo()

    var simulateScan = true
    var simulateNetwork = false

    var scanBadgeReceives = false
    var scanTripReceives = false
    var scanPackageReceives = false


}