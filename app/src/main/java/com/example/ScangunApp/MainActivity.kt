package com.example.ScangunApp

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.example.ScangunApp.Fragments.PackagesFragmentMCDirections
import com.example.ScangunApp.Fragments.ScanTripProcessFragmentDirections
import com.example.ScangunApp.Fragments.ShippingResourceListFragmentDirections
import com.example.ScangunApp.StateMachineLogic.ShippingApplicationMenu
import com.example.ScangunApp.StateMachineLogic.StateMachine

class MainActivity : AppCompatActivity() {

    private lateinit var navController: NavController
    private var backButtonEnabled =  false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.setDisplayHomeAsUpEnabled(false)


        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment

        navController = navHostFragment.navController

    }

    fun DisplayEnableActionBar(state:Boolean)
    {
        val actionBar = supportActionBar
        if (actionBar != null)
        {
            actionBar.setDisplayHomeAsUpEnabled(state)
            backButtonEnabled = state
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        try {

            if (StateMachine.currentMenu ==ShippingApplicationMenu.SelectOrganization &&
                StateMachine.appKeyInfo.organization != "")
            {
                StateMachine.currentMenu = ShippingApplicationMenu.ScanTripId
            }

            val direction = when (StateMachine.currentMenu) {
                ShippingApplicationMenu.ScanTripId -> {
                    ScanTripProcessFragmentDirections.actionScanTripProcessFragmentToShippingResourceListFragment()
                }
                ShippingApplicationMenu.ScanPackages -> {
                    PackagesFragmentMCDirections.actionPackagesFragmentMCToScanTripProcessFragment()
                }
                ShippingApplicationMenu.SelectOrganization ->
                {
                    ShippingResourceListFragmentDirections.actionShippingResourceListFragmentToScanTripProcessFragment()
                }
                else -> {
                    return false
                }

            }

            Log.i("Back Button", item.toString())
            navController.navigate(direction)
            return true
       }
        catch (e:Exception)
        {
            Log.e("Back Button", e.message.toString())
        }


        return false
    }


    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp()
    }



}