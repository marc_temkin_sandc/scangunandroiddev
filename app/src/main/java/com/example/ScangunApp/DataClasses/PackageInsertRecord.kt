package com.example.ScangunApp.DataClasses

import android.os.Parcel
import android.os.Parcelable

data class PackageInsertRecord(
    val TripId: Int, val OrganizationId: Int,
    val ContainerName: String?, val LpnId:Int, val UserId: Int, var Found:Boolean, val Position:Int): Parcelable {


    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readBoolean(),
        parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(TripId)
        parcel.writeInt(OrganizationId)
        parcel.writeString(ContainerName)
        parcel.writeInt(LpnId)
        parcel.writeInt(UserId)
        parcel.writeBoolean(Found)
        parcel.writeInt(Position)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PackageInsertRecord> {
        override fun createFromParcel(parcel: Parcel): PackageInsertRecord {
            return PackageInsertRecord(parcel)
        }

        override fun newArray(size: Int): Array<PackageInsertRecord?> {
            return arrayOfNulls(size)
        }
    }


}