package com.example.ScangunApp.DataClasses

// Stores the key fields needed throughout the navigation of screens
data class AppKeyInfo(var employeeId: String     = "", var organization: String = "", var tripId: String = "")
