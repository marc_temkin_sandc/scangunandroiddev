package com.example.ScangunApp.Fragments

import android.util.Log
import android.view.MenuItem
import androidx.appcompat.app.ActionBar
import androidx.fragment.app.Fragment

open class FragmentWithToolbar : Fragment() {

    protected var toolBar: ActionBar? = null
    fun setToolbar(tb: ActionBar?)
    {
        toolBar = tb
    }



}