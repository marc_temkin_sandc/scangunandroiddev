package com.example.ScangunApp.Fragments


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.fragment.app.ListFragment
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.example.ScangunApp.DataClasses.PackageInsertRecord
import com.example.ScangunApp.DataClasses.ShippingPackageHeader
import com.example.ScangunApp.MainActivity
import com.example.ScangunApp.R
import com.example.ScangunApp.Singletons.CollectionsForLists
import com.example.ScangunApp.StateMachineLogic.ShippingApplicationMenu
import com.example.ScangunApp.StateMachineLogic.StateMachine


class PackagesFragmentMC() : ListFragment() {

    // Controls
    lateinit var list: ListView
    lateinit var list_head: ListView
    private lateinit var adapter: PackagesFragmentCustomAdapter
    private lateinit var HeaderAdapter: PackagesFragmentCustomAdapterHeader

    private var dataModelHeaders: ArrayList<ShippingPackageHeader>? = null
    private var toolBar: ActionBar? = null
    private var viewReference: View? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val view: View = inflater.inflate(R.layout.packagemc_list_fragment, container, false)
        StateMachine.currentMenu = ShippingApplicationMenu.ScanPackages
        if (toolBar != null)
            toolBar!!.title = "Select the container"

        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val host: MainActivity = this.host as MainActivity
        val tb: ActionBar? = host.supportActionBar
        host.DisplayEnableActionBar(true)
        toolBar = tb


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewReference = view
        list = view.findViewById<ListView>(R.id.listDetail)
        list_head = view.findViewById<ListView>(android.R.id.list)

        val items = CollectionsForLists.pkgsList.toTypedArray()
        adapter = PackagesFragmentCustomAdapter(items, this.requireContext(), this)
        list.adapter = adapter

        dataModelHeaders = ArrayList<ShippingPackageHeader>()
        dataModelHeaders!!.add(
            ShippingPackageHeader(
                "Position",
                "Container",
                "LpnId",
                "Package Found"
            )
        )

        HeaderAdapter =
            PackagesFragmentCustomAdapterHeader(dataModelHeaders!!, this.requireContext())
        list_head.adapter = HeaderAdapter


    }

    fun proceedToItem(position: Int): Boolean {
        if (viewReference == null) {
            Log.e("PackagesFragment", "View not available")
            return false
        }
        if (position < 0 && position >= CollectionsForLists.pkgsList.size) {
            Log.e("PackagesFragment", "User requested out-of-range item in the list")
            return false
        }
        val nav: NavController = viewReference!!.findNavController()
        val appKey = StateMachine.appKeyInfo
        val orgs =
            CollectionsForLists.orgsList.filter { q -> q.Code == StateMachine.appKeyInfo.organization }
        val orgId: Int = orgs.first().Id
        val tripId: Int = appKey.tripId.toInt()

        val userId: Int = appKey.employeeId.toInt()



        val pkg = CollectionsForLists.pkgsList[position]
        var containerName = pkg.ContainerName
        var lpnId = pkg.LpnId


        val pkgRecord = pkg.let {
            PackageInsertRecord(
                tripId,
                orgId,
                containerName,
                lpnId,
                userId,
                false,
                pkg.position
            )
        }


        Log.i("nav", "Found navigation")
        val action =
            PackagesFragmentMCDirections.actionPackagesFragmentMCToScanPackageFragment(
                pkgRecord
            )

        nav.navigate(action)
        return true

    }

}

