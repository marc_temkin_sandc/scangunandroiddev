package com.example.ScangunApp.Fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.ActionBar
import com.example.ScangunApp.MainActivity
import com.example.ScangunApp.R

class OrganizationsHostFragment : FragmentWithToolbar() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val host: MainActivity = this.host as MainActivity
        host.DisplayEnableActionBar(true)
        val tb: ActionBar? = host.supportActionBar
        this.setToolbar(tb)

    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.organization_mc_host, container, true)

        if (toolBar != null)
            toolBar!!.title = "Select the packages"

        return view
    }

    override fun onDestroy() {
        super.onDestroy()
    }





}