package com.example.ScangunApp.Fragments


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.fragment.app.ListFragment
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.example.ScangunApp.DataClasses.OrganizationHeader
import com.example.ScangunApp.DataClasses.PackageInsertRecord
import com.example.ScangunApp.DataClasses.ShippingPackageHeader
import com.example.ScangunApp.MainActivity
import com.example.ScangunApp.R
import com.example.ScangunApp.Singletons.CollectionsForLists
import com.example.ScangunApp.StateMachineLogic.ShippingApplicationMenu
import com.example.ScangunApp.StateMachineLogic.StateMachine


class OrganizationsFragmentMC() : ListFragment() {

    // Controls
    lateinit var list: ListView
    lateinit var list_head: ListView
    private lateinit var adapter: OrganizationsFragmentCustomAdapter
    private lateinit var HeaderAdapter: OrganizationsFragmentCustomAdapterHeader

    private var dataModelHeaders: ArrayList<OrganizationHeader>? = null
    private var toolBar: ActionBar? = null
    private var viewReference: View? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val view: View = inflater.inflate(R.layout.organizationmc_list_fragment, container, false)
        StateMachine.currentMenu = ShippingApplicationMenu.SelectOrganization
        if (toolBar != null)
            toolBar!!.title = "Select the organization"

        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val host: MainActivity = this.host as MainActivity
        val tb: ActionBar? = host.supportActionBar
        host.DisplayEnableActionBar(true)
        toolBar = tb


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewReference = view
        list = view.findViewById<ListView>(R.id.listDetail)
        list_head = view.findViewById<ListView>(android.R.id.list)

        val items = CollectionsForLists.orgsList.toTypedArray()
        adapter = OrganizationsFragmentCustomAdapter(items, this.requireContext(), this)
        list.adapter = adapter

        dataModelHeaders = ArrayList<OrganizationHeader>()
        dataModelHeaders!!.add(
            OrganizationHeader(
                "Position",
                "Name",
                "Code",
                "Selected"
            )
        )

        HeaderAdapter =
            OrganizationsFragmentCustomAdapterHeader(dataModelHeaders!!, this.requireContext())
        list_head.adapter = HeaderAdapter


    }

    fun proceedToItem(position: Int): Boolean {
        if (viewReference == null) {
            Log.e("OrganizationFragment", "View not available")
            return false
        }
        if (position < 0 && position >= CollectionsForLists.orgsList.size) {
            Log.e("OrganizationFragment", "User requested out-of-range item in the list")
            return false
        }
        val nav: NavController = viewReference!!.findNavController()

        StateMachine.appKeyInfo.organization = CollectionsForLists.orgsList[position].Code
        StateMachine.currentMenu = ShippingApplicationMenu.ScanTripId

        val action = OrganizationListFragmentDirections.actionOrganizationListFragmentToShippingResourceListFragment()

        nav.navigate(action)
        return true

    }

}

