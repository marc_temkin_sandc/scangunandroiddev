package com.example.ScangunApp.Fragments

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.CheckBox
import android.widget.TextView
import com.example.ScangunApp.DataClasses.Organization
import com.example.ScangunApp.DataClasses.OrganizationHeader
import com.example.ScangunApp.DataClasses.ShippingPackage
import com.example.ScangunApp.R
import com.example.ScangunApp.StateMachineLogic.StateMachine

class OrganizationsFragmentCustomAdapter(
    private val dataSet: Array<Organization>, mContext: Context,
    private val myListFragment: OrganizationsFragmentMC
) :
    ArrayAdapter<Any?>(mContext, R.layout.organization_detail_row, dataSet) {


    private class ViewHolder {
        lateinit var position: TextView
        lateinit var txtOrgName: TextView
        lateinit var txtCode: TextView
        lateinit var checkBox: CheckBox

    }

    override fun getCount(): Int {
        return dataSet.size
    }

    override fun getItem(position: Int): Organization {
        return dataSet[position]
    }

    override fun getView(
        position: Int,
        _convertView: View?,
        parent: ViewGroup
    ): View {
        var convertView = _convertView
        val viewHolder: ViewHolder
        val result: View
        if (convertView == null) {
            viewHolder = ViewHolder()
            convertView =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.package_detail_row, parent, false)
            viewHolder.position = convertView.findViewById(R.id.Slno)
            viewHolder.txtOrgName = convertView.findViewById(R.id.one)
            viewHolder.txtCode = convertView.findViewById(R.id.two)
            viewHolder.checkBox = convertView.findViewById(R.id.checkBox)
            result = convertView
            convertView.tag = viewHolder
        } else {
            viewHolder = convertView.tag as ViewHolder
            result = convertView
        }

        // Initialize the fields in the current row
        val checkBox: CheckBox = viewHolder.checkBox
        val item: Organization = getItem(position)
        viewHolder.position.text = (position + 1).toString()
        viewHolder.txtOrgName.text = item.Code
        viewHolder.txtCode.text = item.Id.toString()

        checkBox.isChecked = item.Selected
        checkBox.tag = position

        checkBox.setOnClickListener(View.OnClickListener { v ->
            val currentPos: Int = v.tag as Int
            val dm: Organization = dataSet[currentPos]
            StateMachine.appKeyInfo.organization = dm.Code
            dataSet.forEach {
                it.Selected = false
            }
            dm.Selected = !dm.Selected
            notifyDataSetChanged()
        })

        return result
    }


}



