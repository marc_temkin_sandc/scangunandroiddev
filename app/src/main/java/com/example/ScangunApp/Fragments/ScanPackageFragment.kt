package com.example.ScangunApp.Fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.android.volley.RequestQueue
import com.android.volley.toolbox.*
import com.example.ScangunApp.DataClasses.PackageInsertRecord
import com.example.ScangunApp.MainActivity
import com.example.ScangunApp.R
import com.example.ScangunApp.Singletons.CollectionsForLists
import com.example.ScangunApp.StateMachineLogic.ShippingApplicationMenu
import com.example.ScangunApp.StateMachineLogic.StateMachine


class ScanPackageFragment : ScanFragment() {
    lateinit var packInfo: PackageInsertRecord

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        arguments?.let {
            packInfo = it.getParcelable("PackageInfo")!!
        }
        val host: MainActivity = this.host as MainActivity
        host.DisplayEnableActionBar(false)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun SetView(inflater: LayoutInflater, container: ViewGroup?): View {
        return inflater.inflate(R.layout.fragment_scan_badge, container, false)
    }

    override fun InitStateMachineField(decodedData: String) {
        if (StateMachine.scanPackageReceives) {
            if (packInfo.ContainerName != decodedData) {
                Log.e("ScanPackageActivity", "Container Name does not match scanned data")
            } else
                Log.i("ScanPackageActivity", decodedData)
        }
    }

    override fun InitializeScanField(view: View) {
        scanField = view.findViewById(R.id.scanBadgeTextField)
        scanField.text = packInfo.ContainerName
        scanButton = view.findViewById(R.id.clickScanBadgeBtn)
        continueButton = view.findViewById(R.id.clickScanBadgeContinue)

    }

    override fun InitScan(view: View) {
        Log.i("Interaction", "InitScan method - You clicked the Init Scan button!")

        var containerName = ""
        if (!scanField.text.isNullOrBlank())
            containerName = scanField.text.toString()

        if (containerName != packInfo.ContainerName) {
            Log.e("Mismatch scan", "Expected container name not scanned")
        }
        StateMachine.currentMenu = ShippingApplicationMenu.SelectOrganization

        val action =
            ScanPackageFragmentDirections.actionScanPackageFragmentToPackagesFragmentMC()


        // Call process to insert packages

        if (StateMachine.simulateNetwork) {
            val packargs =
                "${packInfo.OrganizationId}/${packInfo.TripId}/${packInfo.LpnId}/${packInfo.UserId}"
            Log.i("ScanPackageFragment", packargs)

        } else {
            // See: https://learning.oreilly.com/library/view/android-application-development/9781785886195/ch12s10.html#ch12lvl2sec372
            val queue: RequestQueue = Volley.newRequestQueue(context)

            val pathToResource =resources.getString(R.string.WebAppPath)
                //resources.getString(R.string.WebAppPath)
            val url = pathToResource +
                    "loadlist/api/package/${packInfo.OrganizationId}/${packInfo.TripId}/${packInfo.LpnId}/${packInfo.UserId}"

            val stringReq = object : StringRequest(
                Method.PUT, url,
                // The positive callback
                { response ->
                    Log.i("PutResponse", response.toString())
                    try {

                        CollectionsForLists.UpdatePackages(packInfo.Position)
                        view.findNavController().navigate(action)

                    } catch (e: Exception) {
                        Log.i("error", e.toString())
                    }
                    //your response
                },
                // The error callback
                {
                    Log.i("error", it.toString())
                    // Need to post error message to nav back controller

                    it.printStackTrace()
                    view.findNavController().navigate(action)
                }

            ) {
                override fun getHeaders(): MutableMap<String, String> {
                    val headers = HashMap<String, String>()
                    headers["Content-Type"] = "application/json"
                    return headers
                }

                override fun getBody(): ByteArray {
                    return "\"${packInfo.ContainerName}\"".toByteArray()
                }
            }

            queue.add(stringReq)


        }

    }

    override fun setStatusBarPrompt(): String {
        return "Scan Container Here"
    }
}