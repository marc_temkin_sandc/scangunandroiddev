package com.example.ScangunApp.Fragments

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.example.ScangunApp.DataClasses.OrganizationHeader
import com.example.ScangunApp.DataClasses.ShippingPackageHeader
import com.example.ScangunApp.R


class  OrganizationsFragmentCustomAdapterHeader (private val dataSet: ArrayList<*>, mContext: Context) :
    ArrayAdapter<Any?>(mContext, R.layout.organization_header_row, dataSet) {
    private class ViewHolder {
        lateinit var position: TextView
        lateinit var txtOrgName: TextView
        lateinit var txtCode: TextView
    }

    override fun getCount(): Int {
        return dataSet.size
    }

    override fun getItem(position: Int): OrganizationHeader {
        return dataSet[position] as OrganizationHeader
    }

    override fun getView(
        position: Int,
        _convertView: View?,
        parent: ViewGroup
    ): View {
        var convertView = _convertView
        val viewHolder: ViewHolder
        val result: View
        if (convertView == null) {
            viewHolder = ViewHolder()
            convertView =
                LayoutInflater.from(parent.context).inflate(R.layout.organization_header_row, parent, false)
            viewHolder.position = convertView.findViewById(R.id.SlnoHeader)
            viewHolder.txtOrgName = convertView.findViewById(R.id.oneHeader)
            viewHolder.txtCode= convertView.findViewById(R.id.twoHeader)
            result = convertView
            convertView.tag = viewHolder
        } else {
            viewHolder = convertView.tag as ViewHolder
            result = convertView
        }

        // Initialize the fields in the current row
        val item: OrganizationHeader = getItem(position)
        viewHolder.txtOrgName.text = item.Name
        viewHolder.txtCode.text = item.Code
        viewHolder.position.text = item.position


        return result
    }
}

