package com.example.ScangunApp.Fragments

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.CheckBox
import android.widget.TextView
import com.example.ScangunApp.DataClasses.ShippingPackage
import com.example.ScangunApp.R

class PackagesFragmentCustomAdapter(
    private val dataSet: Array<ShippingPackage>, mContext: Context,
    private val myListFragment: PackagesFragmentMC
) :
    ArrayAdapter<Any?>(mContext, R.layout.package_detail_row, dataSet) {


    private class ViewHolder {
        lateinit var position: TextView
        lateinit var txtCountry: TextView
        lateinit var txtCapital: TextView
        lateinit var checkBox: CheckBox
    }

    override fun getCount(): Int {
        return dataSet.size
    }

    override fun getItem(position: Int): ShippingPackage {
        return dataSet[position]
    }

    override fun getView(
        position: Int,
        _convertView: View?,
        parent: ViewGroup
    ): View {
        var convertView = _convertView
        val viewHolder: ViewHolder
        val result: View
        if (convertView == null) {
            viewHolder = ViewHolder()
            convertView =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.package_detail_row, parent, false)
            viewHolder.position = convertView.findViewById(R.id.Slno)
            viewHolder.txtCountry = convertView.findViewById(R.id.one)
            viewHolder.txtCapital = convertView.findViewById(R.id.two)
            viewHolder.checkBox = convertView.findViewById(R.id.checkBox)
            result = convertView
            convertView.tag = viewHolder
        } else {
            viewHolder = convertView.tag as ViewHolder
            result = convertView
        }

        // Initialize the fields in the current row
        val checkBox: CheckBox = viewHolder.checkBox
        val item: ShippingPackage = getItem(position)
        viewHolder.txtCountry.text = item.ContainerName
        viewHolder.txtCapital.text = item.LpnId.toString()
        viewHolder.position.text = item.position.toString()
        checkBox.isChecked = item.Found



        checkBox.tag = position

        checkBox.setOnClickListener(View.OnClickListener { v ->
            val currentPos: Int = v.tag as Int
            val dm: ShippingPackage = dataSet[currentPos]
            if (this.myListFragment.proceedToItem(currentPos)) {
                // TODO: Should verify here that the clicked item
                // is found in the persisted results
                // See ScanPackageFragment: 100-101
                dm.Found = !dm.Found
                notifyDataSetChanged()
            }

        })

        return result
    }


}



