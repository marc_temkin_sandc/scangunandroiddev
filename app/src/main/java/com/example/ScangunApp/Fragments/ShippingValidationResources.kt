package com.example.ScangunApp.Fragments

import android.content.res.Resources
import com.example.ScangunApp.DataClasses.AppKeyInfo
import com.example.ScangunApp.DataClasses.resourceRec
import com.example.ScangunApp.ReferenceLists.IReferenceListUtilities
import com.example.ScangunApp.StateMachineLogic.ShippingApplicationMenu
import com.example.ScangunApp.StateMachineLogic.StateMachine
import com.google.gson.Gson
import java.util.*

class ShippingValidationResources {
    private val shippingResources: TreeMap<String, String> = TreeMap<String, String>()

    val ShippingResources: TreeMap<String, String>
        get() = shippingResources


    fun getShippingResourcesList(): List<resourceRec> {

        val menuOptions = arrayOf("ScanBadge", "SelectOrganization", "ScanTripId",  "ExitApp")
        val recs: MutableList<resourceRec> =
            shippingResources.
            filter{ (key, _) -> menuOptions.contains(key)}.
            toList().
            sortedBy { it -> menuOptions.indexOf(it.first) }.
            map { resourceRec(it.first, it.second) }.toMutableList()

        // Add the default record
        recs.add(0, resourceRec("",""))

        val appKeyInfo:AppKeyInfo = StateMachine.appKeyInfo

        // Display the scanned Employee ID
        if (!appKeyInfo.employeeId.isNullOrBlank())
        {
            val scanBadgeRec =
                recs.first { r -> r.name == "ScanBadge" }

                scanBadgeRec.value =
                  scanBadgeRec.value.replace("...", appKeyInfo.employeeId)
        }

        if (!appKeyInfo.organization.isNullOrBlank())
        {
            val scanBadgeRec =
                recs.first { r -> r.name == "SelectOrganization" }

                scanBadgeRec.value =
                    scanBadgeRec.value.replace("...", appKeyInfo.organization)
        }

        if (!appKeyInfo.tripId.isNullOrBlank())
        {
            val scanBadgeRec =
                recs.first { r -> r.name == "ScanTripId" }

            scanBadgeRec.value =
                scanBadgeRec.value.replace("...", appKeyInfo.tripId)
        }


        var position = 0
        for (rec in recs)
            rec.position = position++

        return recs
    }

    fun CreateResources(refList: IReferenceListUtilities, resources: Resources) {

        try {
            val pathToFile = "ShippingValidationResources.json"
            val json: String = refList.GetShippingOrganizationsText(
                resources, pathToFile
            )

            val gson = Gson()
            val resourcesAsArray = gson.fromJson(json, Array<resourceRec>::class.java)

            shippingResources.put("", "")
            for (res in resourcesAsArray)
                shippingResources.put(res.name, res.value)



        } catch (e: Exception) {
            throw e
        }

    }
}



