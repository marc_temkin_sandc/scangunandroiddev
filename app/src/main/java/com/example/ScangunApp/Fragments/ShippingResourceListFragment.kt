package com.example.ScangunApp.Fragments

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ScangunApp.DataClasses.AppKeyInfo
import com.example.ScangunApp.DataClasses.resourceRec
import com.example.ScangunApp.ViewModels.ShippingResourceListViewModel
import com.example.ScangunApp.MainActivity
import com.example.ScangunApp.R
import com.example.ScangunApp.StateMachineLogic.ShippingApplicationMenu
import com.example.ScangunApp.StateMachineLogic.StateMachine
import com.example.ScangunApp.Fragments.ShippingResourceListFragmentDirections as ShippingResourceListFragmentDirections



private const val TAG = "ShipResListFragment"

class ShippingResourceListFragment : FragmentWithToolbar(){
    private lateinit var shippingResourceRecyclerView: RecyclerView
    private var adapter: ShippingResourceListFragment.ShippingResourceAdapter? = null

    private val menuData:ShippingMenuData = ShippingMenuData()

    companion object {
        fun newInstance(): ShippingResourceListFragment {
            return  ShippingResourceListFragment()
        }
    }



    private val resourceListViewModel: ShippingResourceListViewModel by lazy {
        ViewModelProvider(this).get(ShippingResourceListViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        resourceListViewModel.initializeRecords(this.resources)
        if (StateMachine.currentMenu == ShippingApplicationMenu.None)
            StateMachine.currentMenu = ShippingApplicationMenu.ScanBadge

        val tb:ActionBar? =
        (this.host as MainActivity).supportActionBar
        this.setToolbar(tb)
        //toolBar!!.displayOptions.
        Log.d(TAG, "Total resource: ${resourceListViewModel.size}")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_org_list, container, false)
        shippingResourceRecyclerView =
            view.findViewById(R.id.org_recycler_view) as RecyclerView
        shippingResourceRecyclerView.layoutManager = LinearLayoutManager(context)

        updateUI()

        val host: MainActivity = this.host as MainActivity
        host.DisplayEnableActionBar(false)

        if (toolBar != null)
            toolBar!!.title = menuData.statusBarText()
        return view
    }



    private fun updateUI() {
        val items = resourceListViewModel.recs
        adapter = ShippingResourceAdapter(items)
        shippingResourceRecyclerView.adapter = adapter
    }

    inner class ShippingMenuData()
    {
        fun statusBarText():String
        {
            val appKeyInfo:AppKeyInfo = StateMachine.appKeyInfo
            val message:String =
            if (appKeyInfo.employeeId == "") "Select option 1 to continue"
            else if (appKeyInfo.organization == "") "Select option 2 to continue"
            else "Select an option from the menu"

            return message
        }

    }

    private inner class ShippingResourceAdapter(var recs: List<resourceRec>?) :
    RecyclerView.Adapter<ShipResourceHolder>(){
        private val ITEM_VIEW_TYPE_HEADER = 0
        private val ITEM_VIEW_TYPE_ITEM = 1

        private var currentViewTypePosition:Int =ITEM_VIEW_TYPE_HEADER

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShippingResourceListFragment.ShipResourceHolder {

            val resourceId:Int = if (currentViewTypePosition == ITEM_VIEW_TYPE_HEADER)
                R.layout.list_item_menu_header
            else
                R.layout.list_item_org

            val view = layoutInflater.inflate(resourceId, parent, false)

            return ShipResourceHolder(view)
        }

        override fun getItemViewType(position: Int): Int {
            currentViewTypePosition = if (position == 0) ITEM_VIEW_TYPE_HEADER else ITEM_VIEW_TYPE_ITEM
            return super.getItemViewType(position)
        }

        override fun onBindViewHolder(holder: ShippingResourceListFragment.ShipResourceHolder, position: Int) {
            if (recs != null) {
                val organization = recs!![position]
                if (currentViewTypePosition == ITEM_VIEW_TYPE_ITEM)
                    holder.bind(organization)

            }
        }

        override fun getItemCount() = recs!!.size

    }

    private inner class ShipResourceHolder(view : View) :
    RecyclerView.ViewHolder(view), View.OnClickListener
    {


        private var rec: resourceRec? = null
        private val positionField: TextView = itemView.findViewById(R.id.position)
        private val orgNameField: TextView = itemView.findViewById(R.id.containerName)
        private val codeField: TextView = itemView.findViewById(R.id.code)
        private val menuOptions = arrayOf(ShippingApplicationMenu.ScanBadge,
            ShippingApplicationMenu.SelectOrganization,
            ShippingApplicationMenu.ScanTripId)

        init {
            itemView.setOnClickListener(this)
        }

        fun bind(res: resourceRec)
        {
            this.rec = res
            positionField.text = this.rec!!.position.toString()
            orgNameField.text = this.rec!!.value
            codeField.text = this.rec!!.name

            val suppressAfter = menuOptions.indexOf(StateMachine.currentMenu) +1
            if (StateMachine.currentMenu == ShippingApplicationMenu.ScanBadge)
            {
                if (this.rec!!.position == 0)
                {
                    // The header
                    positionField.setTextColor(Color.WHITE)
                    orgNameField.setTextColor(Color.WHITE)
                    codeField.setTextColor(Color.WHITE)
                }

                else if (this.rec!!.position > suppressAfter)
                {
                    positionField.isEnabled = false
                    orgNameField.isEnabled = false
                    codeField.isEnabled = false
                }
                else
                {
                    positionField.isEnabled = true
                    orgNameField.isEnabled = true
                    codeField.isEnabled = true

                }
            }
        }

        // Normally the State Machine moves to the next action.
        // However the initial statements will check if the user
        // has selected a different item that is.
        override fun onClick(v: View) {
            val selectedRec = this.rec


            val selectedMenu:ShippingApplicationMenu =
            ShippingApplicationMenu.valueOf(selectedRec?.name.toString())

            val selectedMenuPosition:Int = menuOptions.indexOf(selectedMenu)
            val nextMenuPosition:Int = menuOptions.indexOf(StateMachine.currentMenu)

            if (selectedMenuPosition < nextMenuPosition || nextMenuPosition == -1)
            {
                StateMachine.currentMenu = selectedMenu
            }


            val action =
                when (StateMachine.currentMenu) {
                ShippingApplicationMenu.ScanBadge ->
                    ShippingResourceListFragmentDirections
                        .actionShippingResourceListFragmentToScanBadgeProcessFragment()

               ShippingApplicationMenu.SelectOrganization ->
                   ShippingResourceListFragmentDirections.actionShippingResourceListFragmentToOrganizationsFragmentMC()

                    //ShippingResourceListFragmentDirections.actionShippingResourceListFragmentToOrganizationListFragment()
                ShippingApplicationMenu.ScanTripId ->
                    ShippingResourceListFragmentDirections
                        .actionShippingResourceListFragmentToScanTripProcessFragment()

                else -> null
            }

            StateMachine.clearScanFlags()

            val setStates =
            when (StateMachine.currentMenu)
            {
                ShippingApplicationMenu.ScanBadge ->
                    StateMachine.scanBadgeReceives = true
                ShippingApplicationMenu.ScanTripId ->
                    StateMachine.scanTripReceives = true
                ShippingApplicationMenu.ScanPackage ->
                    StateMachine.scanPackageReceives = true
                else -> null
            }
            Log.i("SetStates", setStates.toString())

            if (action != null) {
                v.findNavController().navigate(action)
            }

            if (activity != null && StateMachine.currentMenu == ShippingApplicationMenu.ExitApp)
            {
                activity!!.finishAffinity()
            }

        }
    }

}