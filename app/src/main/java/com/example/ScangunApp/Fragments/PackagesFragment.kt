package com.example.ScangunApp.Fragments

import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.DragEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ScangunApp.DataClasses.PackageInsertRecord
import com.example.ScangunApp.DataClasses.ShippingPackage
import com.example.ScangunApp.MainActivity
import com.example.ScangunApp.R
import com.example.ScangunApp.Singletons.CollectionsForLists
import com.example.ScangunApp.StateMachineLogic.ShippingApplicationMenu
import com.example.ScangunApp.StateMachineLogic.StateMachine
import com.example.ScangunApp.ViewModels.PackageListViewModel

private const val TAG = "PackagesListFragment"

class PackagesFragment : FragmentWithToolbar() {


    private lateinit var orgRecyclerView: RecyclerView
    private var adapter: PackagesAdapter? = null

    // Need to create a central object
    /*private val menuData: ShippingResourceListFragment.ShippingMenuData =
        ShippingResourceListFragment.ShippingMenuData()*/

    private val packagesListViewModel: PackageListViewModel by lazy {
        ViewModelProvider(this).get(PackageListViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        packagesListViewModel.initializeRecords(this.resources, this.requireContext())
        Log.d(TAG, "Total ShippingPackages: ${packagesListViewModel.packageRecs.size}")


        val host: MainActivity = this.host as MainActivity
        val tb: ActionBar? = host.supportActionBar
        host.DisplayEnableActionBar(true)
        this.setToolbar(tb)

    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_org_list, container, false)

        orgRecyclerView =
            view.findViewById(R.id.org_recycler_view) as RecyclerView
        orgRecyclerView.layoutManager = LinearLayoutManager(context)

        updateUI()

        if (toolBar != null)
            toolBar!!.title = "Select the container"

        return view
    }

    private fun updateUI() {
        val items = packagesListViewModel.packageRecs
        adapter = PackagesAdapter(items)
        orgRecyclerView.adapter = adapter
    }

    private inner class PackageHolder(view: View) : RecyclerView.ViewHolder(view),
        View.OnClickListener {
        private var pkg: ShippingPackage? = null
        private val positionField: TextView = itemView.findViewById(R.id.position)
        private val containerNameField: TextView = itemView.findViewById(R.id.containerName)
        private val foundStatusField: CheckBox? = itemView.findViewById(R.id.packageFound)

        init {
            itemView.setOnClickListener(this)

               }

        fun bind(shippingPackage: ShippingPackage) {

                this.pkg = shippingPackage
                positionField.text = this.pkg!!.position.toString()
                containerNameField.text = this.pkg!!.ContainerName
                if (foundStatusField != null) {
                    foundStatusField.isChecked = this.pkg!!.Found
                }

        }


        override fun onClick(view: View) {
            val appKey = StateMachine.appKeyInfo
            val orgs =
                CollectionsForLists.orgsList.filter { q -> q.Code == StateMachine.appKeyInfo.organization }
            val orgId: Int = orgs.first().Id
            val tripId: Int = appKey.tripId.toInt()
            var containerName = this.pkg?.ContainerName
            val userId: Int = appKey.employeeId.toInt()
            var lpnId = this.pkg?.LpnId


            if (containerName == null)
                containerName = ""

            if (lpnId == null)
                lpnId = 0


            val pkgRecord = this.pkg?.let {
                PackageInsertRecord(
                    tripId,
                    orgId,
                    containerName,
                    lpnId,
                    userId,
                    false,
                    this.pkg!!.position
                )
            }
            if (pkgRecord != null) {
                // If there are still packages to process stay in this screen

                StateMachine.clearScanFlags()
                StateMachine.currentMenu = ShippingApplicationMenu.ScanPackages

            }
        }
    }

    private inner class PackagesAdapter(var shippingPackages: List<ShippingPackage>) :
        RecyclerView.Adapter<PackageHolder>() {
        private val ITEM_VIEW_TYPE_HEADER = 0
        private val ITEM_VIEW_TYPE_ITEM = 1

        private var currentViewTypePosition: Int = ITEM_VIEW_TYPE_HEADER

        private val bold: Typeface = Typeface.create("san-serif", Typeface.BOLD)

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PackageHolder {
            val resourceId: Int = if (currentViewTypePosition == ITEM_VIEW_TYPE_HEADER)
                R.layout.list_item_package_header
            else
                R.layout.list_item_package

            val view = layoutInflater.inflate(resourceId, parent, false)
            return PackageHolder(view)
        }

        override fun getItemViewType(position: Int): Int {
            currentViewTypePosition =
                if (position == 0) ITEM_VIEW_TYPE_HEADER else ITEM_VIEW_TYPE_ITEM
            return super.getItemViewType(position)
        }

        override fun onBindViewHolder(holder: PackageHolder, position: Int) {
            if (position < 0 || position > shippingPackages.size - 1)
                return

            val DispPackage = shippingPackages[position]
            if (currentViewTypePosition == ITEM_VIEW_TYPE_ITEM)
                holder.bind(DispPackage)
        }

        override fun getItemCount() = shippingPackages.size
        fun RedrawHeaders() {
            TODO("Not yet implemented")
        }

    }


    companion object {
        fun newInstance(): PackagesFragment {
            return PackagesFragment()
        }
    }
}
/*
private fun View.setOnScrollChangeListener(packageHolder: PackagesFragment.PackageHolder) {

    Log.i("ScrollEvent","Hello from setOnScrollChangeListener")
}
*/

