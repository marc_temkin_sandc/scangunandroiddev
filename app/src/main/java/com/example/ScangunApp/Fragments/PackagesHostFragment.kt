package com.example.ScangunApp.Fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ScangunApp.MainActivity
import com.example.ScangunApp.R
import com.example.ScangunApp.StateMachineLogic.StateMachine
import com.example.ScangunApp.ViewModels.OrganizationListViewModel

class PackagesHostFragment : FragmentWithToolbar() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val host: MainActivity = this.host as MainActivity
        host.DisplayEnableActionBar(true)
        val tb: ActionBar? = host.supportActionBar
        this.setToolbar(tb)

    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.package_mc_host, container, true)

        if (toolBar != null)
            toolBar!!.title = "Select the packages"

        return view
    }

    override fun onDestroy() {
        super.onDestroy()
    }





}