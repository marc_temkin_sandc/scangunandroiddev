package com.example.ScangunApp.Fragments

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.navigation.findNavController
import com.example.ScangunApp.MainActivity
import com.example.ScangunApp.R
import com.example.ScangunApp.StateMachineLogic.ShippingApplicationMenu
import com.example.ScangunApp.StateMachineLogic.StateMachine
import java.lang.Exception

abstract class ScanFragment : FragmentWithToolbar() {

    /* References to controls, and control properties */
    protected lateinit var scanField: TextView
    protected lateinit var scanButton: Button
    protected lateinit var continueButton: Button
    protected lateinit var scanButtonText: String


    protected val myBroadcastReceiver : BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            //val ub = intent.extras
            if (action == resources.getString(R.string.activity_intent_filter_action)) {
                try {
                    displayScanResult(intent)
                } catch (e: Exception) {
                }
            }
        }
    }

    private fun displayScanResult(initiatingIntent: Intent) {
        /* From Sample Code. Might need later.
        val decodedSource =
          initiatingIntent.getStringExtra(resources.getString(R.string.datawedge_intent_key_source))

        val decodedLabelType =
        initiatingIntent.getStringExtra(resources.getString(R.string.datawedge_intent_key_label_type))
        */
        val decodedData =
            initiatingIntent.getStringExtra(resources.getString(R.string.datawedge_intent_key_data))


        scanField.text = decodedData
        if (!decodedData.isNullOrBlank()) {
            InitStateMachineField(decodedData)
            scanButton.text = scanButtonText
            scanButton.isEnabled = true
        }


    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val host: MainActivity = this.host as MainActivity
        host.DisplayEnableActionBar(false)

        val tb: ActionBar? = host.supportActionBar
        this.setToolbar(tb)

        if (!StateMachine.simulateScan) {
            registerClassReceiver()
        }

    }

    fun registerClassReceiver()
    {
        val filter = IntentFilter()
        filter.addCategory(Intent.CATEGORY_DEFAULT)
        filter.addAction(resources.getString(R.string.activity_intent_filter_action))

        requireActivity().registerReceiver(myBroadcastReceiver, filter)
    }


    override fun onDestroy() {
        super.onDestroy()
        if (!StateMachine.simulateScan) {
            requireActivity().unregisterReceiver(myBroadcastReceiver)
        }
    }

    abstract fun SetView(inflater: LayoutInflater, container: ViewGroup?) : View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =SetView(inflater, container)

        InitializeScanField(view)

        continueButton.isEnabled = false

        this.scanButtonText = scanButton.text.toString()


            scanButton.setOnClickListener {
                Log.i("Interaction", "You clicked the Init Scan button!")
                scanButton.text = "Waiting for scan.."
                scanButton.isEnabled = false
                continueButton.isEnabled = true

            }


            continueButton.setOnClickListener {
                Log.i("Interaction", "You clicked the Continue  button!")
                scanButton.text = scanButtonText
                scanButton.isEnabled = true
                InitScan(view)
            }


        if (toolBar != null)
        {
            toolBar!!.title = setStatusBarPrompt()
        }

        //updateUI()


        return view

    }

    abstract fun InitStateMachineField(decodedData:String)
    abstract fun InitializeScanField(view: View)
    abstract fun InitScan(view: View)

    /**
     *  Used to set the prompt for this fragment
     */
    abstract fun setStatusBarPrompt():String
}