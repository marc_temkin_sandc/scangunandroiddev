package com.example.ScangunApp.Fragments


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.ActionBar
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.ScangunApp.DataClasses.Organization
import com.example.ScangunApp.MainActivity
import com.example.ScangunApp.R
import com.example.ScangunApp.ReferenceLists.ProxyReferenceListUtilities
import com.example.ScangunApp.Singletons.CollectionsForLists
import com.example.ScangunApp.StateMachineLogic.ShippingApplicationMenu
import com.example.ScangunApp.StateMachineLogic.StateMachine
import com.google.gson.Gson

class ScanBadgeProcessFragment : ScanFragment() {

    var scanBadgeReceives= false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val host: MainActivity = this.host as MainActivity
        host.DisplayEnableActionBar(false)
        scanBadgeReceives = true
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return super.onCreateView(inflater, container, savedInstanceState)
    }


    override fun SetView(inflater: LayoutInflater, container: ViewGroup?): View {
        return  inflater.inflate(R.layout.fragment_scan_badge, container, false)
    }


    override fun InitStateMachineField(decodedData: String) {
        if (scanBadgeReceives) {
            // A badge may have leading and trailing X's padding the actual number
            val captureData = decodedData.replace('X', ' ').trim()
            StateMachine.appKeyInfo.employeeId = captureData
            Log.i("ScanBadgeActivity", StateMachine.appKeyInfo.employeeId)
        }
    }

    override fun InitializeScanField(view: View) {
        scanField = view.findViewById(R.id.scanBadgeTextField)
        scanButton = view.findViewById(R.id.clickScanBadgeBtn)
        continueButton = view.findViewById(R.id.clickScanBadgeContinue)

        if (StateMachine.simulateScan) {
            scanField.text = R.string.DummyEmployeeId.toString()
        }

    }

    // Triggered by the Continue button.
    // The scanned value is saved to the StateMachine,
    // the list of Organizations is saved to a singleton class using the
    // functions GetOrgsFromFile, if the network is VisVi, or
    // from GetOrgsFromURL, which calls the internal web server.
    // Each of these functions call the next action as their last steps.
    override fun InitScan(view: View) {

        Log.i("Interaction", "InitScan method - You clicked the Init Scan button!")

        if (!scanField.text.isNullOrBlank()) {
            val captureData = scanField.text.toString().replace('X', ' ').trim()
            StateMachine.appKeyInfo.employeeId = captureData
        }

        StateMachine.currentMenu = ShippingApplicationMenu.SelectOrganization

        // The current broadcast receiver needs to be unregistered prior to moving to
        // the next screen. If this does not occur then the current receiver will still
        // pick up the messages even though the application is focused on a different
        // fragment and fragment class.
        if (!StateMachine.simulateScan)
            scanBadgeReceives = false
            //requireActivity().unregisterReceiver(myBroadcastReceiver)

        val action =
            ScanBadgeProcessFragmentDirections.actionScanBadgeProcessFragmentToShippingResourceListFragment()


        if (StateMachine.simulateNetwork)
        {
            GetOrgsFromFile( "organizations.json",view, action)
        }
        else {
            val pathToResource = resources.getString(R.string.WebAppPath) + "loadlist/api/organizations"
            GetOrgsFromURL(pathToResource, view, action)
        }



    }


    // Use the proxy reference to fill the list of Organizations and
    // then continue to the next screen
    private fun GetOrgsFromFile(pathToResource:String, view: View, action: NavDirections) {
        try {

            val refList =ProxyReferenceListUtilities()
            val json = refList.GetShippingOrganizationsText(
                resources, pathToResource)

            val gson = Gson()
            val orgList = gson.fromJson(json, Array<Organization>::class.java)
            CollectionsForLists.InitOrg(orgList)
            view.findNavController().navigate(action)

        }
        catch (e:Exception)
        {
            throw e
        }
    }

    // Call the webserver directly so that the move to the next screen
    // does not occur until the StringRequest triggers the positive callback.
    // The RequestQueue instance is part of the patten used by the Volley library
    private fun GetOrgsFromURL(pathToResource:String, view: View, action: NavDirections) {


        val queue: RequestQueue = Volley.newRequestQueue(context)
        val url =pathToResource


        val stringReq = StringRequest(
            Request.Method.GET, url,
            // The positive callback
            { response ->
                val gson = Gson()
                val orgList = gson.fromJson(response, Array<Organization>::class.java)
                CollectionsForLists.InitOrg(orgList)
                view.findNavController().navigate(action)

            },
            // The error callback
            {
                Log.e("ScanBadgeProcess", "error with Volley callback")
            }
        )

        queue.add(stringReq)

    }



    /**
     *  Used to set the prompt for this fragment
     */
    override fun setStatusBarPrompt(): String {
        return resources.getString(R.string.ScanBadgeProcess)
    }

}


