package com.example.ScangunApp.Fragments

import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.example.ScangunApp.DataClasses.Organization
import com.example.ScangunApp.MainActivity
import com.example.ScangunApp.ViewModels.OrganizationListViewModel
import com.example.ScangunApp.R
import com.example.ScangunApp.StateMachineLogic.ShippingApplicationMenu
import com.example.ScangunApp.StateMachineLogic.StateMachine


private const val TAG = "OrgListFragment"

class OrganizationListFragment : FragmentWithToolbar(){

    private lateinit var orgRecyclerView: RecyclerView
    private var adapter: OrganizationAdapter? = null


    private val organizationListViewModel: OrganizationListViewModel by lazy {
        ViewModelProvider(this).get(OrganizationListViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (this.context != null) {
            organizationListViewModel.initializeRecords(this.resources, this.requireContext())
            Log.d(TAG, "Total Organizations: ${organizationListViewModel.orgRecs.size}")

        }

        val host: MainActivity = this.host as MainActivity
        host.DisplayEnableActionBar(false)
        val tb:ActionBar? = host.supportActionBar
        this.setToolbar(tb)

    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //return super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater.inflate(R.layout.fragment_org_list, container, false)

        orgRecyclerView =
            view.findViewById(R.id.org_recycler_view) as RecyclerView
        orgRecyclerView.layoutManager = LinearLayoutManager(context)


        updateUI()

        if (toolBar != null)
            toolBar!!.title = "Select the shipping organization"

        return view
    }

    private fun updateUI() {
        val items = organizationListViewModel.orgRecs
        adapter = OrganizationAdapter(items)
        orgRecyclerView.adapter = adapter
    }

    private inner class OrgHolder(view : View)
        :RecyclerView.ViewHolder(view), View.OnClickListener {
        private var organization: Organization? = null
        private val positionField: TextView = itemView.findViewById(R.id.position)
        private val orgNameField:TextView = itemView.findViewById(R.id.containerName)
        private val codeField:TextView = itemView.findViewById(R.id.code)

        init {
            itemView.setOnClickListener(this)
        }

        fun bind(organization: Organization)
        {
            this.organization = organization
            positionField.text = this.organization!!.position.toString()
            orgNameField.text = this.organization!!.Code
            codeField.text = this.organization!!.Id.toString()
        }
        override fun onClick(view: View) {
            if (organization != null) {
                StateMachine.appKeyInfo.organization = organization!!.Code
                Toast.makeText(context, organization!!.Code, Toast.LENGTH_SHORT).show()


                StateMachine.currentMenu = ShippingApplicationMenu.ScanTripId


                val action =
                    OrganizationListFragmentDirections.
                    actionOrganizationListFragmentToShippingResourceListFragment()

                view.findNavController().navigate(action)
            }
        }


    }

    private inner class OrganizationAdapter(var orgs: List<Organization>) :
    RecyclerView.Adapter<OrgHolder>() {
    private val ITEM_VIEW_TYPE_HEADER = 0
    private val ITEM_VIEW_TYPE_ITEM = 1

      private var currentViewTypePosition:Int =ITEM_VIEW_TYPE_HEADER

      private val bold:Typeface = Typeface.create("san-serif", Typeface.BOLD)

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrgHolder {
            val resourceId:Int = if (currentViewTypePosition == ITEM_VIEW_TYPE_HEADER)
                R.layout.list_item_org_header
            else
                R.layout.list_item_org

            val view = layoutInflater.inflate(resourceId, parent, false)
            return OrgHolder(view)
        }

        override fun getItemViewType(position: Int): Int {
            currentViewTypePosition = if (position == 0) ITEM_VIEW_TYPE_HEADER else ITEM_VIEW_TYPE_ITEM
            return super.getItemViewType(position)
        }

        override fun onBindViewHolder(holder: OrgHolder, position: Int) {
            val organization = orgs[position]
            if (currentViewTypePosition == ITEM_VIEW_TYPE_ITEM)
                holder.bind(organization)
        }

        override fun getItemCount() = orgs.size

    }


    companion object {
        fun newInstance(): OrganizationListFragment {
            return  OrganizationListFragment()
        }
    }
}