package com.example.ScangunApp.Fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.navigation.NavDirections
import androidx.navigation.findNavController

import com.example.ScangunApp.R
import com.example.ScangunApp.StateMachineLogic.ShippingApplicationMenu
import com.example.ScangunApp.StateMachineLogic.StateMachine
import com.android.volley.Request
import com.android.volley.RequestQueue

import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.ScangunApp.DataClasses.Organization
import com.example.ScangunApp.DataClasses.ShippingPackage
import com.example.ScangunApp.MainActivity
import com.example.ScangunApp.ReferenceLists.ProxyReferenceListUtilities
import com.example.ScangunApp.Singletons.CollectionsForLists
import com.google.gson.Gson

class ScanTripProcessFragment : ScanFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val host: MainActivity = this.host as MainActivity
        StateMachine.scanTripReceives = true
        StateMachine.currentMenu = ShippingApplicationMenu.ScanTripId

        val tb: ActionBar? = host.supportActionBar
        host.DisplayEnableActionBar(true)
        toolBar = tb

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun SetView(inflater: LayoutInflater, container: ViewGroup?): View {
        return  inflater.inflate(R.layout.fragment_scan_trip, container, false)
    }

    override fun InitStateMachineField(decodedData: String) {
        if (StateMachine.scanTripReceives) {
            StateMachine.appKeyInfo.tripId = decodedData
            Log.i("ScanTripIdActivity", StateMachine.appKeyInfo.tripId)
        }
    }

    override fun InitializeScanField(view: View) {
        scanField = view.findViewById(R.id.scanTripIdTextField)
        scanButton = view.findViewById(R.id.clickScanTripBtn)
        continueButton = view.findViewById(R.id.clickScanTripContinue)

        if (StateMachine.simulateScan)
        {
            scanField.text = "121850"
                //R.string.DummyScanTripId.toString()
        }
    }

    override fun InitScan(view: View) {
        Log.i("Interaction","InitScan method - You clicked the Init Scan button!")

        if (!scanField.text.isNullOrBlank() )
            StateMachine.appKeyInfo.tripId = scanField.text.toString()

        StateMachine.currentMenu = ShippingApplicationMenu.ScanPackages


        //val action =          ScanTripProcessFragmentDirections.actionScanTripProcessFragmentToPackagesFragment()


        val action = ScanTripProcessFragmentDirections.actionScanTripProcessFragmentToPackagesFragmentMC()

        if (!StateMachine.simulateScan)
            StateMachine.scanTripReceives = false

        if (StateMachine.simulateNetwork)
        {
            getTripsFromFile( "SamplePackageRequest.json",view, action)
        }
        else {
            val orgs = CollectionsForLists.orgsList.filter { q -> q.Code == StateMachine.appKeyInfo.organization}
            if (orgs.isNotEmpty()) {

                val pathToResource = resources.getString(R.string.WebAppPath) + "loadlist/api/loadlist"
                val orgId = orgs.first().Id
                val tripid = StateMachine.appKeyInfo.tripId

                getTripsFromURL(
                    "$pathToResource/$orgId/$tripid",
                    view,
                    action
                )


            }
        }


    }

    // Use the proxy reference to fill the list of Organizations and
    // then continue to the next screen
    private fun getTripsFromFile(pathToResource:String, view: View, action: NavDirections) {
        try {

            val refList = ProxyReferenceListUtilities()
            val json = refList.GetPackagesText(
                resources, pathToResource)

            val gson = Gson()
            val pkgList = gson.fromJson(json, Array<ShippingPackage>::class.java)
            CollectionsForLists.InitPackages(pkgList)
            view.findNavController().navigate(action)

        }
        catch (e:Exception)
        {
            throw e
        }
    }


    // Call the webserver directly so that the move to the next screen
    // does not occur until the StringRequest triggers the positive callback.
    // The RequestQueue instance is part of the patten used by the Volley library
    private fun getTripsFromURL(pathToResource:String, view: View, action: NavDirections) {


        val queue: RequestQueue = Volley.newRequestQueue(context)
        val url =pathToResource


        val stringReq = StringRequest(
            Request.Method.GET, url,
            // The positive callback
            { response ->
                val gson = Gson()
                val pkgList = gson.fromJson(response, Array<ShippingPackage>::class.java)
                CollectionsForLists.InitPackages(pkgList)
                view.findNavController().navigate(action)
            },
            // The error callback
            {
                Log.e("Scan Trip", "No Trips found")
                Toast.makeText(context,"No Trips found for: " + StateMachine.appKeyInfo.tripId.toString(), Toast.LENGTH_LONG).show()
            }

        )
        queue.add(stringReq)



    }


    /**
     * Used to set the prompt for this fragment
     */
    override fun setStatusBarPrompt():String {
        return resources.getString(R.string.ScanTripIdPrompt)
    }
}