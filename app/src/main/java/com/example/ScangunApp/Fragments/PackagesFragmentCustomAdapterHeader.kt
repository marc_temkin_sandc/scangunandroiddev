package com.example.ScangunApp.Fragments


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.example.ScangunApp.DataClasses.ShippingPackageHeader
import com.example.ScangunApp.R


class  PackagesFragmentCustomAdapterHeader (private val dataSet: ArrayList<*>, mContext: Context) :
    ArrayAdapter<Any?>(mContext, R.layout.package_header_row, dataSet) {
    private class ViewHolder {
        lateinit var position: TextView
        lateinit var txtContainer: TextView
        lateinit var txtLpnId: TextView
        lateinit var txtIsFound: TextView
    }

    override fun getCount(): Int {
        return dataSet.size
    }

    override fun getItem(position: Int): ShippingPackageHeader {
        return dataSet[position] as ShippingPackageHeader
    }

    override fun getView(
        position: Int,
        _convertView: View?,
        parent: ViewGroup
    ): View {
        var convertView = _convertView
        val viewHolder: ViewHolder
        val result: View
        if (convertView == null) {
            viewHolder = ViewHolder()
            convertView =
                LayoutInflater.from(parent.context).inflate(R.layout.package_header_row, parent, false)
            viewHolder.position = convertView.findViewById(R.id.SlnoHeader)
            viewHolder.txtContainer = convertView.findViewById(R.id.oneHeader)
            viewHolder.txtLpnId = convertView.findViewById(R.id.twoHeader)
            viewHolder.txtIsFound =   convertView.findViewById(R.id.pickHeader)
            result = convertView
            convertView.tag = viewHolder
        } else {
            viewHolder = convertView.tag as ViewHolder
            result = convertView
        }

        // Initialize the fields in the current row
        val item: ShippingPackageHeader = getItem(position)
        viewHolder.txtContainer.text = item.Container
        viewHolder.txtLpnId.text = item.LpnIdTitle
        viewHolder.position.text = item.position
        viewHolder.txtIsFound.text = item.IsFoundTitle


        return result
    }
}

