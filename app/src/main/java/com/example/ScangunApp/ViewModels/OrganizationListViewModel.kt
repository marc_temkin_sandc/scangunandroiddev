package com.example.ScangunApp.ViewModels

import android.content.Context
import android.content.res.Resources
import androidx.lifecycle.ViewModel
import com.example.ScangunApp.DataClasses.Organization
import com.example.ScangunApp.Singletons.CollectionsForLists


class OrganizationListViewModel: ViewModel() {


    var orgRecs = mutableListOf<Organization>()

    fun initializeRecords(resources: Resources, context: Context) {

            orgRecs = CollectionsForLists.orgsList.toMutableList()
            orgRecs.add(0,     Organization(-1, "-1", -1))

        }

    }