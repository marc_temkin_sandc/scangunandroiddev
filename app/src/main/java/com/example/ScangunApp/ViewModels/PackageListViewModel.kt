package com.example.ScangunApp.ViewModels

import android.content.Context
import android.content.res.Resources
import androidx.lifecycle.ViewModel
import com.example.ScangunApp.DataClasses.ShippingPackage
import com.example.ScangunApp.Singletons.CollectionsForLists


class PackageListViewModel : ViewModel() {

    var packageRecs = mutableListOf<ShippingPackage>()

    fun initializeRecords(resources: Resources, context: Context)
    {

        packageRecs = CollectionsForLists.pkgsList.toMutableList()
        packageRecs.add(0,     ShippingPackage(-1, "-1", -1, true))

    }
}