package com.example.ScangunApp.ViewModels

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import com.example.ScangunApp.Fragments.ShippingResourceListFragment
import com.example.ScangunApp.Fragments.ShippingValidationResources
import com.example.ScangunApp.DataClasses.resourceRec
import com.example.ScangunApp.ReferenceLists.ProxyReferenceListUtilities

class ShippingResourceListViewModel : ViewModel(){
    var recs: List<resourceRec>? = null
    var size: Int = 0

    fun initializeRecords(
        resources: Resources
    ) {
        val shipResources = ShippingValidationResources()
        shipResources.CreateResources(ProxyReferenceListUtilities(), resources)
        recs = shipResources.getShippingResourcesList()
    }

}
